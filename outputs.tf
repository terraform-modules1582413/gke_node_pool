output "taints" {
  value = google_container_node_pool.main.node_config[0].taint
}
output "network_tag" {
  value = google_container_node_pool.main.name
}
output "selectors" {
  value = {
    name = google_container_node_pool.main.name
  }
}
