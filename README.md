<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_container_node_pool.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_node_pool) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gke_cluster_name"></a> [gke\_cluster\_name](#input\_gke\_cluster\_name) | n/a | `string` | n/a | yes |
| <a name="input_gke_custom_node_pool_disk_size_gb"></a> [gke\_custom\_node\_pool\_disk\_size\_gb](#input\_gke\_custom\_node\_pool\_disk\_size\_gb) | n/a | `number` | `16` | no |
| <a name="input_gke_custom_node_pool_disk_type"></a> [gke\_custom\_node\_pool\_disk\_type](#input\_gke\_custom\_node\_pool\_disk\_type) | n/a | `string` | `"pd-standard"` | no |
| <a name="input_gke_custom_node_pool_image_type"></a> [gke\_custom\_node\_pool\_image\_type](#input\_gke\_custom\_node\_pool\_image\_type) | n/a | `string` | `"UBUNTU_CONTAINERD"` | no |
| <a name="input_gke_custom_node_pool_labels"></a> [gke\_custom\_node\_pool\_labels](#input\_gke\_custom\_node\_pool\_labels) | n/a | `map(string)` | n/a | yes |
| <a name="input_gke_custom_node_pool_machine_type"></a> [gke\_custom\_node\_pool\_machine\_type](#input\_gke\_custom\_node\_pool\_machine\_type) | n/a | `string` | `"e2-small"` | no |
| <a name="input_gke_custom_node_pool_max_node_count"></a> [gke\_custom\_node\_pool\_max\_node\_count](#input\_gke\_custom\_node\_pool\_max\_node\_count) | n/a | `number` | `1` | no |
| <a name="input_gke_custom_node_pool_min_node_count"></a> [gke\_custom\_node\_pool\_min\_node\_count](#input\_gke\_custom\_node\_pool\_min\_node\_count) | n/a | `number` | `1` | no |
| <a name="input_gke_custom_node_pool_name"></a> [gke\_custom\_node\_pool\_name](#input\_gke\_custom\_node\_pool\_name) | n/a | `string` | n/a | yes |
| <a name="input_gke_custom_node_pool_pods_range_name"></a> [gke\_custom\_node\_pool\_pods\_range\_name](#input\_gke\_custom\_node\_pool\_pods\_range\_name) | n/a | `string` | n/a | yes |
| <a name="input_gke_custom_node_pool_preemptible"></a> [gke\_custom\_node\_pool\_preemptible](#input\_gke\_custom\_node\_pool\_preemptible) | n/a | `bool` | `false` | no |
| <a name="input_gke_custom_node_pool_spot"></a> [gke\_custom\_node\_pool\_spot](#input\_gke\_custom\_node\_pool\_spot) | n/a | `bool` | `false` | no |
| <a name="input_gke_custom_node_pool_taints"></a> [gke\_custom\_node\_pool\_taints](#input\_gke\_custom\_node\_pool\_taints) | n/a | <pre>list(object({<br>    key    = string<br>    value  = string<br>    effect = string<br>  }))</pre> | n/a | yes |
| <a name="input_gke_default_nodes_network_tag"></a> [gke\_default\_nodes\_network\_tag](#input\_gke\_default\_nodes\_network\_tag) | n/a | `string` | n/a | yes |
| <a name="input_gke_location"></a> [gke\_location](#input\_gke\_location) | n/a | `string` | n/a | yes |
| <a name="input_gke_sa_email"></a> [gke\_sa\_email](#input\_gke\_sa\_email) | n/a | `string` | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_selectors"></a> [selectors](#output\_selectors) | n/a |
| <a name="output_taints"></a> [taints](#output\_taints) | n/a |
<!-- END_TF_DOCS -->