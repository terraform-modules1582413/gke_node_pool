resource "google_container_node_pool" "main" {
  project  = var.gcp_project_id
  name     = var.gke_custom_node_pool_name
  location = var.gke_location
  cluster  = var.gke_cluster_name

  initial_node_count = 1

  autoscaling {
    min_node_count = var.gke_custom_node_pool_min_node_count
    max_node_count = var.gke_custom_node_pool_max_node_count
  }

  node_config {
    service_account = var.gke_sa_email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = var.gke_custom_node_pool_labels

    resource_labels = var.gke_custom_node_pool_labels

    taint = var.gke_custom_node_pool_taints

    machine_type = var.gke_custom_node_pool_machine_type
    image_type   = var.gke_custom_node_pool_image_type
    disk_type    = var.gke_custom_node_pool_disk_type
    disk_size_gb = var.gke_custom_node_pool_disk_size_gb
    preemptible  = var.gke_custom_node_pool_preemptible
    spot         = var.gke_custom_node_pool_spot
    tags         = [var.gke_default_nodes_network_tag, var.gke_custom_node_pool_name]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  network_config {
    enable_private_nodes = true
    create_pod_range     = false
    pod_range            = var.gke_custom_node_pool_pods_range_name
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}
