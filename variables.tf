variable "project_environment" {
  type = string
}

variable "gcp_project_id" {
  type = string
}

variable "gke_location" {
  type = string
}
variable "gke_cluster_name" {
  type = string
}
variable "gke_sa_email" {
  type = string
}
variable "gke_global_network_tag" {
  type = string
}
variable "gke_custom_node_pool_name" {
  type = string
}
variable "gke_custom_node_pool_pods_range_name" {
  type = string
}
variable "gke_custom_node_pool_min_node_count" {
  type    = number
  default = 1
}
variable "gke_custom_node_pool_max_node_count" {
  type    = number
  default = 1
}
variable "gke_custom_node_pool_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
}
variable "gke_custom_node_pool_labels" {
  type = map(string)
}
variable "gke_custom_node_pool_machine_type" {
  type    = string
  default = "e2-small"
}
variable "gke_custom_node_pool_image_type" {
  type    = string
  default = "UBUNTU_CONTAINERD"
}
variable "gke_custom_node_pool_disk_type" {
  type    = string
  default = "pd-standard"
}
variable "gke_custom_node_pool_disk_size_gb" {
  type    = number
  default = 16
}
variable "gke_custom_node_pool_preemptible" {
  type    = bool
  default = false
}
variable "gke_custom_node_pool_spot" {
  type    = bool
  default = false
}
